﻿/*Equipo 9 
• GARCÍA GÓMEZ JOSÉ MANUEL 
• RIVERA SANTISTEVAN SEBASTIAN
*/ 

use master
go 

/*==============================================================*/
/* Creacion de la base de datos SISTEMAPROFESIONISTAS           */
/*==============================================================*/
create database SISTEMA_PROFESIONISTAS
go

use SISTEMA_PROFESIONISTAS
go 

/*==============================================================*/
/* Table: PROFESIONISTA                                         */
/*==============================================================*/
create table PROFESIONISTA (
   ID_PROF              varchar(20)                    not null,
   NOMBRE_P             char(40)                       not null,
   GRADO_AC             char(40)                       null,
   FECH_NAC             date                           null,
   DIRECCION_P          varchar(100)                   null
  
)
go 

sp_primarykey PROFESIONISTA, ID_PROF
go

/*==============================================================*/
/* Table: EMPRESA                                               */
/*==============================================================*/
create table EMPRESA (
   ID_EMP               varchar(20)                    not null,
   NOMBRE_E             char(30)                       not null,
   DIRECCION_E          varchar(100)                   null,
  
  
)
go

sp_primarykey EMPRESA, ID_EMP
go

/*==============================================================*/
/* Table: AREA_TRABAJO                                          */
/*==============================================================*/
create table AREA_TRABAJO (
   ID_AT                varchar(20)                    not null,
   NOMBRE_AT            varchar(40)                    not null
   
)
go 

sp_primarykey AREA_TRABAJO, ID_AT
go 

/*==============================================================*/
/* Table: TRABAJA                                               */
/*==============================================================*/
create table TRABAJA (
   ID_PROF              varchar(20)                    not null,
   ID_EMP               varchar(20)                    not null,
   PUESTO_E             varchar(30)                    not null,
   SALARIO              money                          null
               
)
go

sp_primarykey TRABAJA, ID_PROF, ID_EMP
go

/*==============================================================*/
/* Table: TIENE                                                 */
/*==============================================================*/
create table TIENE (
   ID_EMP               varchar(20)                    not null,
   ID_AT                varchar(20)                    not null
 
)
go

sp_primarykey TIENE, ID_EMP, ID_AT
go

/*==============================================================*/
/* Table: UNIVERSIDAD                                           */
/*==============================================================*/
create table UNIVERSIDAD (
   ID_UNIV              varchar(20)                    not null,
   NOMBRE_U             char(30)                       not null,
   DOMICILIO_U          varchar(100)                    null
   )
go

sp_primarykey UNIVERSIDAD, ID_UNIV
go


/*==============================================================*/
/* Table: SE_GRADUO                                             */
/*==============================================================*/
create table SE_GRADUO (
   ID_UNIV              varchar(20)                    not null,
   ID_PROF              varchar(20)                    not null
)
go

sp_primarykey SE_GRADUO, ID_UNIV, ID_PROF
go


/*==============================================================*/
/* Table: CARRERA                                               */
/*==============================================================*/
create table CARRERA (
   ID_CARR              varchar(30)                    not null,
   NOMBRE_C             char(40)                       not null
  )
go

sp_primarykey CARRERA, ID_CARR
go


/*==============================================================*/
/* Table: SE_IMPARTE                                            */
/*==============================================================*/
create table SE_IMPARTE (
   ID_CARR              varchar(30)                    not null,
   ID_UNIV              varchar(20)                    not null
)
go

sp_primarykey SE_IMPARTE, ID_CARR,ID_UNIV
go


/*==============================================================*/
/* Creacion de llaves foraneas segun corresponda                */
/*==============================================================*/


sp_foreignkey TRABAJA, PROFESIONISTA, ID_PROF
go

sp_foreignkey TRABAJA, EMPRESA, ID_EMP
go 

sp_foreignkey SE_GRADUO, PROFESIONISTA, ID_PROF
go 

sp_foreignkey SE_GRADUO, UNIVERSIDAD, ID_UNIV
go

sp_foreignkey TIENE, EMPRESA, ID_EMP
go

sp_foreignkey TIENE, AREA_TRABAJO, ID_AT
go

sp_foreignkey SE_IMPARTE, UNIVERSIDAD, ID_UNIV
go

sp_foreignkey SE_IMPARTE, CARRERA, ID_CARR
go

/*==============================================================*/
/* Creacion de Index para implementar integridad de entidad     */
/*==============================================================*/

create unique index PROFESIONISTA_PK
  on PROFESIONISTA(ID_PROF)
go

create unique index EMPRESA_PK
  on EMPRESA(ID_EMP)
go

create index TRABAJA_FK
  on TRABAJA(ID_PROF, ID_EMP) 
go

create unique index UNIVERSIDAD_PK
  on UNIVERSIDAD(ID_UNIV)
go

create index SE_GRADUO_FK
  on SE_GRADUO(ID_PROF,ID_UNIV)
go

create unique index AREA_TRABAJO_PK
  on AREA_TRABAJO(ID_AT)
go

create index TIENE_FK
  on TIENE(ID_EMP, ID_AT)
go 


create unique index CARRERA_PK
  on CARRERA(ID_CARR)
go

create index SE_IMPARTE_FK
  on SE_IMPARTE(ID_UNIV, ID_CARR)
go 


/*==============================================================*/
/* Insert values: PROFESIONISTA                                 */
/*==============================================================*/

insert into PROFESIONISTA
  values('PROF001','Juan Martinez Perez','Maestria',"1956/09/15" ,'Concepcion Beistegui #986,Col. del Valle')
go
insert into PROFESIONISTA
  values('PROF002','Diana Hernandez Diaz','Doctorado',"1970/02/28",'Av. de la Paz #12,Col.Coyoacan')
go
insert into PROFESIONISTA
  values('PROF003','Elizabeth Benitez Altamirano','Doctorado',"1981/07/09",'Miguel Angel de Quevedo #87,Col. Coyoacan')
go
insert into PROFESIONISTA
  values('PROF004','Pilar Echeverria Lopez','Maestria',"1973/04/25",'Miguel Laurent #14, Col. del Valle')
go
insert into PROFESIONISTA
  values('PROF005','Anna Fernandez Cabrera','Maestria',"1984/12/08",'Pennsylvanya #4, Col. Napoles')
go
insert into PROFESIONISTA
  values('PROF006','Sebastian Esquiel Montoya','Doctorado',"1953/08/19",'Michoacan #49, Col. Condesa')
go
insert into PROFESIONISTA
  values('PROF007','Jose Rodriguez Mcnot','Licenciatura',"1964/12/17",'Rio Magdalena #2143,Col. San Jerónimo')
go
insert into PROFESIONISTA
  values('PROF008','Fernando Gutierrez Mungia','Licenciatura',"1968/05/06",'Roberto Gayol #897, Col. del Valle')
go
insert into PROFESIONISTA
  values('PROF009','Roberto Palazuelos Santiago','Doctorado',"1979/07/10",'Tintoreto #234, Col. Algarín')
go
insert into PROFESIONISTA
  values('PROF010','Emiliano Beas Esquier','Licenciatura',"1954/01/13",'Recreo #984, Col. Polanco')
go

/*==============================================================*/
/* Insert values: EMPRESA                                       */
/*==============================================================*/

insert into EMPRESA
  values('EMP020','Canon','Centenario 638, Col. Niños Heroes')
go

insert into EMPRESA
  values('EMP021','Sony','V. carranza #21, Col. Centro Cuahutemoc')
go

insert into EMPRESA
  values('EMP022','Pepsi','Hoonduras #24, Col. Centro')
go

insert into EMPRESA
  values('EMP023','Google','Elias Zamora Verduzco #1502, Col. Casino de la Feria')
go

insert into EMPRESA
  values('EMP024','Oracle','Armonia 381, Col. Lomas Verdes')
go

insert into EMPRESA
  values('EMP025','Reebok','Roberto Suarez #456, Col. Ramon Serrano')
go

insert into EMPRESA
  values('EMP026','Skype','Parque Insurgentes #56, Col. Tamala')
go

insert into EMPRESA
  values('EMP027','Amazon','Prol. Hidalgo #9340, Col. Colinas del Carmen')
go

insert into EMPRESA
  values('EMP028','Virgin','Benito Juarez #7653, Col. Centro')
go

insert into EMPRESA
  values('EMP029','IKEA','Lopez Mateos #346, Col. Las Joyas')
go



/*==============================================================*/
/* Insert values: TRABAJA                                       */
/*==============================================================*/
/*   ID_PROF              varchar(20)                    not null,
   ID_EMP               varchar(20)                    not null,
   PUESTO_E             varchar(30)                    not null,
   SALARIO              money                          null*/
insert into TRABAJA
  values('PROF001','EMP020','Director', $70000.00)
go

insert into TRABAJA
  values('PROF002','EMP021','Coordinador', $350000.00)
go

insert into TRABAJA
  values('PROF003','EMP022','Gerente', $50000.00)
go

insert into TRABAJA
  values('PROF004','EMP023','Director', $70000.00)
go

insert into TRABAJA
  values('PROF005','EMP024','Coordinador', $35000.00)
go

insert into TRABAJA
  values('PROF006','EMP025','Gerente', $50000.00)
go

insert into TRABAJA
  values('PROF007','EMP026','Director', $70000.00)
go

insert into TRABAJA
  values('PROF008','EMP027','Coordinador', $35000.00)
go

insert into TRABAJA
  values('PROF009','EMP028','Gerente', $50000.00)
go

insert into TRABAJA
  values('PROF010','EMP029','Director', $70000.00)
go

/*==============================================================*/
/* Insert values: UNIVERDIDAD                                   */
/*==============================================================*/

insert into UNIVERSIDAD
  values('UNIV301','BUAP','Agustin Lara #69-B, Col. La Piragua')
go

insert into UNIVERSIDAD
  values('UNIV302','UASLP','20 de Noviembre #12, Col. El Reposo')
go

insert into UNIVERSIDAD
  values('UNIV303','UPN','Aldama #789, Col Centro')
go

insert into UNIVERSIDAD
  values('UNIV304','UAZ','Daniel Soto #438, Col. Flor de Pña')
go

insert into UNIVERSIDAD
  values('UNIV305','IPN','Independencia #3569, Col. El Desenganio')
go

insert into UNIVERSIDAD
  values('UNIV306','ITESM','Matamoros #225, Col. Lazaro Cardenas')
go

insert into UNIVERSIDAD
  values('UNIV307','UNAM','Jesus Carranza, Col. Fraccionamiento Costa')
go

insert into UNIVERSIDAD
  values('UNIV308','UANL','5 de Mayo, Col. Oaxaca')
go

insert into UNIVERSIDAD
  values('UNIV309','UDLAP','Miguel Aleman 2, Col. Costa Verde')
go

insert into UNIVERSIDAD
  values('UNIV310','COLMEX','Mariano Arista #54, Col. Centro Tuxtepec')
go

/*==============================================================*/
/* Insert values: SE_GRADUO                                     */
/*==============================================================*/

insert into SE_GRADUO 
  values('UNIV304','PROF005')
go

insert into SE_GRADUO 
  values('UNIV303','PROF006')
go

insert into SE_GRADUO 
  values('UNIV302','PROF007')
go

insert into SE_GRADUO 
  values('UNIV301','PROF008')
go

insert into SE_GRADUO 
  values('UNIV310','PROF009')
go

insert into SE_GRADUO 
  values('UNIV309','PROF0010')
go

insert into SE_GRADUO 
  values('UNIV308','PROF001')
go

insert into SE_GRADUO 
  values('UNIV307','PROF002')
go

insert into SE_GRADUO 
  values('UNIV306','PROF003')
go

insert into SE_GRADUO 
  values('UNIV305','PROF04')
go

/*==============================================================*/
/* Insert values: AREA_TRABAJO                                  */
/*==============================================================*/

insert into AREA_TRABAJO
  values('AT001','Sector financiero y monetario')
go

insert into AREA_TRABAJO
  values('AT002','Desarrollo Industrial')
go

insert into AREA_TRABAJO
  values('AT003','Educacion')
go

insert into AREA_TRABAJO
  values('AT004','Energia')
go

insert into AREA_TRABAJO
  values('AT005','Desarrollo Sustentable')
go

insert into AREA_TRABAJO
  values('AT006','Biodiversidad')
go

insert into AREA_TRABAJO
  values('AT007','Logistica y movilidad')
go

insert into AREA_TRABAJO
  values('AT008','Recursos mineros')
go

insert into AREA_TRABAJO
  values('AT009','Agricultura y desarrollo rural')
go

insert into AREA_TRABAJO
  values('AT010','TIC')
go

/*==============================================================*/
/* Insert values: CARRERAS                                      */
/*==============================================================*/

insert into CARRERA
  values('CARR501','Ingenieria en Computacion')
go

insert into CARRERA
  values('CARR502','Ingenieria Quimica')
go

insert into CARRERA
  values('CARR503','Finanzas')
go

insert into CARRERA
  values('CARR504','Ingeniaria de Minas y Metalurgia')
go

insert into CARRERA
  values('CARR505','Ciencia de Materiales Sustentables')
go

insert into CARRERA
  values('CARR506','Ingenieria en Energias Renovables')
go

insert into CARRERA
  values('CARR507','Ciencias Sociales')
go

insert into CARRERA
  values('CARR508','Psicologia')
go

insert into CARRERA
  values('CARR509','Pedagogia')
go

insert into CARRERA
  values('CARR510','Ciencias Politicas')
go


/*==============================================================*/
/* Insert values: TIENE                                         */
/*==============================================================*/

insert into TIENE
  values('EMP022','AT009')
go 

insert into TIENE
  values('EMP024','AT007')
go 

insert into TIENE
  values('EMP026','AT005')
go 

insert into TIENE
  values('EMP028','AT003')
go 

insert into TIENE
  values('EMP020','AT001')
go 

insert into TIENE
  values('EMP021','AT010')
go 

insert into TIENE
  values('EMP023','AT008')
go 

insert into TIENE
  values('EMP025','AT006')
go 

insert into TIENE
  values('EMP027','AT004')
go 

insert into TIENE
  values('EMP029','AT002')
go 

/*==============================================================*/
/* Insert values: SE_IMPARTE                                    */
/*==============================================================*/

insert into SE_IMPARTE
  values('CARR505','UNIV305')
go
insert into SE_IMPARTE
  values('CARR505','UNIV306')
go
insert into SE_IMPARTE
  values('CARR505','UNIV307')
go
insert into SE_IMPARTE
  values('CARR505','UNIV309')
go
insert into SE_IMPARTE
  values('CARR505','UNIV301')
go



insert into SE_IMPARTE
  values('CARR506','UNIV305')
go
insert into SE_IMPARTE
  values('CARR506','UNIV306')
go
insert into SE_IMPARTE
  values('CARR506','UNIV307')
go
insert into SE_IMPARTE
  values('CARR506','UNIV309')
go
insert into SE_IMPARTE
  values('CARR506','UNIV301')
go


insert into SE_IMPARTE
  values('CARR504','UNIV305')
go
insert into SE_IMPARTE
  values('CARR504','UNIV306')
go
insert into SE_IMPARTE
  values('CARR504','UNIV307')
go
insert into SE_IMPARTE
  values('CARR504','UNIV309')
go
insert into SE_IMPARTE
  values('CARR504','UNIV301')
go



insert into SE_IMPARTE
  values('CARR502','UNIV305')
go
insert into SE_IMPARTE
  values('CARR502','UNIV306')
go
insert into SE_IMPARTE
  values('CARR502','UNIV307')
go
insert into SE_IMPARTE
  values('CARR502','UNIV309')
go
insert into SE_IMPARTE
  values('CARR502','UNIV301')
go



insert into SE_IMPARTE
  values('CARR501','UNIV305')
go
insert into SE_IMPARTE
  values('CARR501','UNIV306')
go
insert into SE_IMPARTE
  values('CARR501','UNIV307')
go
insert into SE_IMPARTE
  values('CARR501','UNIV309')
go
insert into SE_IMPARTE
  values('CARR501','UNIV301')
go



insert into SE_IMPARTE
  values('CARR503','UNIV302')
go
insert into SE_IMPARTE
  values('CARR503','UNIV303')
go
insert into SE_IMPARTE
  values('CARR503','UNIV304')
go
insert into SE_IMPARTE
  values('CARR503','UNIV308')
go
insert into SE_IMPARTE
  values('CARR503','UNIV310')
go




insert into SE_IMPARTE
  values('CARR507','UNIV302')
go
insert into SE_IMPARTE
  values('CARR507','UNIV303')
go
insert into SE_IMPARTE
  values('CARR507','UNIV304')
go
insert into SE_IMPARTE
  values('CARR507','UNIV308')
go
insert into SE_IMPARTE
  values('CARR507','UNIV310')
go




insert into SE_IMPARTE
  values('CARR508','UNIV302')
go
insert into SE_IMPARTE
  values('CARR508','UNIV303')
go
insert into SE_IMPARTE
  values('CARR508','UNIV304')
go
insert into SE_IMPARTE
  values('CARR508','UNIV308')
go
insert into SE_IMPARTE
  values('CARR508','UNIV310')
go



insert into SE_IMPARTE
  values('CARR509','UNIV302')
go
insert into SE_IMPARTE
  values('CARR509','UNIV303')
go
insert into SE_IMPARTE
  values('CARR509','UNIV304')
go
insert into SE_IMPARTE
  values('CARR509','UNIV308')
go
insert into SE_IMPARTE
  values('CARR509','UNIV310')
go




insert into SE_IMPARTE
  values('CARR510','UNIV302')
go
insert into SE_IMPARTE
  values('CARR510','UNIV303')
go
insert into SE_IMPARTE
  values('CARR510','UNIV304')
go
insert into SE_IMPARTE
  values('CARR510','UNIV308')
go
insert into SE_IMPARTE
  values('CARR510','UNIV310')
go

/*==============================================================*/
/*Creación de triggers para mantener la integridad de los datos*/
/*==============================================================*/

/*==============================================================*/
/* Trigger para mantener la integridad referencial en la tabla  */
/*  TRABAJA                                                     */
/*==============================================================*/
create trigger insert_trabaja
  on TRABAJA
  for insert  as 
  if (select count(*) from TRABAJA, PROFESIONISTA, EMPRESA, inserted
      where((TRABAJA.ID_PROF = inserted.ID_PROF) and (PROFESIONISTA.ID_PROF = inserted.ID_PROF)
      and (TRABAJA.ID_EMP = inserted.ID_EMP) and (EMPRESA.ID_EMP = inserted.ID_EMP))
      )!=@@rowcount
  begin rollback transaction
  print '¡Failed Insert! ("no related data was found")'
end
else
    print 'Successful Insertion'
go  

/*==============================================================*/
/* Trigger para mantener la integridad referencial en la tabla  */
/*  SE_GRADUO                                                   */
/*==============================================================*/
create trigger insert_graduo
  on SE_GRADUO
  for insert as
  if(select count(*) from SE_GRADUO, PROFESIONISTA, UNIVERSIDAD, inserted
      where((SE_GRADUO.ID_PROF = inserted.ID_PROF) and (PROFESIONISTA.ID_PROF = inserted.ID_PROF)
      and (SE_GRADUO.ID_UNIV = inserted.ID_UNIV) and (UNIVERSIDAD.ID_UNIV = inserted.ID_UNIV))
      )!=@@rowcount
  begin rollback transaction
  print '¡Failed Insert! ("no related data was found")'
end
else
    print 'Successful Insertion'
go

/*==============================================================*/
/* Trigger para mantener la integridad referencial en la tabla  */
/*  TIENE                                                       */
/*==============================================================*/
create trigger insert_tiene
  on TIENE
  for insert as
  if(select count(*) from TIENE, EMPRESA, AREA_TRABAJO, inserted
      where((TIENE.ID_EMP = inserted.ID_EMP) and (EMPRESA.ID_EMP = inserted.ID_EMP)
      and (TIENE.ID_AT = inserted.ID_AT) and (AREA_TRABAJO.ID_AT = inserted.ID_AT))
      )!=@@rowcount
  begin rollback transaction
  print '¡Failed Insert! ("no related data was found")'
end
else
    print 'Successful Insertion'

go

/*==============================================================*/
/* Trigger para mantener la integridad referencial en la tabla  */
/*   SE_IMPARTE                                                 */
/*==============================================================*/
create trigger insert_imparte
  on SE_IMPARTE
  for insert as
  if(select count(*) 
      from SE_IMPARTE,UNIVERSIDAD, CARRERA, inserted
      where((SE_IMPARTE.ID_CARR = inserted.ID_CARR) and (CARRERA.ID_CARR = inserted.ID_CARR)
      and (SE_IMPARTE.ID_UNIV = inserted.ID_UNIV) and (UNIVERSIDAD.ID_UNIV = inserted.ID_UNIV))
      )!=@@rowcount
  begin rollback transaction
  print '¡Failed Insert! ("no related data was found")'
end
else
    print 'Successful Insertion'
go


/*==============================================================*/
/* Trigger para realizar un borrado en cascada  en las tablas   */
/*  TRABAJA y TIENE a la hora de borrar un registro en la tabla */
/*    EMPRESA, manteniendo así  la integridad referencial    	  */
/*==============================================================*/

create trigger deleteEmpresa
  on EMPRESA
  for delete as
  if (select count(*)
    from deleted, TRABAJA, TIENE
    where (TRABAJA.ID_EMP= deleted.ID_EMP) 
    and (TIENE.ID_EMP=deleted.ID_EMP) )>0
  begin
  delete TRABAJA
    from deleted, TRABAJA
    where TRABAJA.ID_EMP=deleted.ID_EMP 
  delete TIENE
    from deleted, TIENE
    where TIENE.ID_EMP=deleted.ID_EMP
  print'Borrdado en cascada'
  end 
go

/*==============================================================*/
/* Trigger para realizar un borrado en cascada  en las tabla    */
/*  TRABAJA, a la hora de borrar un registro en la tabla        */
/*  PROFESIONISTA, manteniendo así  la integridad referencial   */
/*==============================================================*/

create trigger deletedProfesionista
  on PROFESIONISTA
  for delete as 
  delete TRABAJA
  from deleted, TRABAJA
  where TRABAJA.ID_PROF=deleted.ID_PROF
go

/*==============================================================*/
/* Trigger para realizar un borrado en cascada  en las tabla    */
/*  TIENE, a la hora de borrar un registro en la tabla          */
/*  AREA_TRABAJO, manteniendo así  la integridad referencial    */
/*==============================================================*/ 
create trigger deleted_AT
  on AREA_TRABAJO
  for delete as 
  delete TIENE
  from deleted, TIENE
  where TIENE.ID_AT=deleted.ID_AT
go


/*==============================================================*/
/* Trigger restrictivo, a la hora de tratar de borrar un        */
/* registro  en la tabla CARRERA, esta no podrá ser borrada ya  */
/* que está asociada a un profesionista donde este se graduado  */
/* de  esa universidad donde se impartía dicha carrera          */
/*==============================================================*/

create trigger deletedCarrera
  on CARRERA 
  for delete as
  if( select count(*)
    from deleted, SE_IMPARTE, SE_GRADUO
    where((SE_IMPARTE.ID_CARR= deleted.ID_CARR) AND
          (SE_IMPARTE.ID_UNIV= SE_GRADUO.ID_UNIV))
    )>0
    begin
    rollback transaction
    print 'No se puede borrar carrera porque esta asociado a un PROFESIONISTA'
    end
go

/*==============================================================*/
/* Trigger restrictivo, a la hora de tratar de borrar un        */
/* registro  en la tabla AREA_TRABAJO, esta no podrá ser        */
/* borrada ya que está asociada a una empresa donde está a un   */
/* Tenga esa área de trabajo                                    */
/*==============================================================*/

create trigger deleted_AT
  on AREA_TRABAJO
  for delete as
  if( select count(*)
    from deleted, TIENE
    where((TIENE.ID_AT= deleted.ID_AT))
    )>0
    begin
    rollback transaction
    print 'No se puede borrar AREA_TRABAJO porque esta asociado a una EMPRESA'
    end
go

/*==============================================================*/
/* Creacion de procesos almacenados                             */
/*==============================================================*/

/*==============================================================*/
/* Proceso almacenado para insertar una nueva carrera           */
/*==============================================================*/
create procedure sp_inserta_carrera
  (@idcarr varchar(30) = NULL, @nombrec char(40)=NULL)
  as
  declare @msg varchar(40)
  if @idcarr is NULL or @nombrec is NULL
    begin
      raiserror 40204
      "No se ejecuto correctamente:
       exec proc_insertar_carrera @idcarr, @nombrec"
      return
    end
  else
    insert into CARRERA
      values(@idcarr, @nombrec)
      select @msg = "El registro de inserto correctamente"
      print @msg
    return
go
----ejecuta del proceso
--exec sp_inserta_carrera @idmarcac, @nombre  

/*==============================================================*/
/* Proceso almacenado para insertar una nueva área de trabajo   */
/*==============================================================*/
  create procedure sp_inserta_at
  (@id_at varchar(30) = NULL, @nombreat char(40)=NULL)
  as
  declare @msg varchar(40)
  if @id_at is NULL or @nombreat is NULL
    begin
      raiserror 40204
      "No se ejecuto correctamente:
       exec proc_insertar_carrera @id_at, @nombreat"
      return
    end
  else
    insert into AREA_TRABAJO
      values(@id_at, @nombreat)
      select @msg = "El registro de inserto correctamente"
      print @msg
    return
go
----ejecuta del proceso
--exec sp_inserta_at @id_at, @nombreat  


/*==============================================================*/
/* Proceso almacenado para actualizar ID_CARR en tabla CARRERA  */
/*==============================================================*/
create procedure sp_actualiza_idcarr
  (@valold varchar(30)=NULL, @valnew varchar(30)= NULL)
  as
  declare @msg varchar(40)
  if @valold is NULL or @valnew is NULL
    begin
      raiserror 40205
      "La ejecucion de este procedimiento es:
          exec sp_actualiza_idcarr @valold, @valnew"
      return
      end
    else
      begin
        update CARRERA set ID_CARR = @valnew
        where ID_CARR = @valold
        select @msg = "Se actualizo correctamente el registro"
        print @msg
      end
go
----ejecuta del proceso
--exec sp_actualiza_idcarr @valold, @valnew 



/*==============================================================*/
/* Proceso almacenado para actualizar NOMBRE_C en tabla CARRERA */
/*==============================================================*/
create procedure sp_actualiza_nombrec
  (@valold varchar(30)=NULL, @valnew varchar(30)= NULL)
  as
  declare @msg varchar(40)
  if @valold is NULL or @valnew is NULL
    begin
      raiserror 40205
      "La ejecucion de este procedimiento es:
          exec sp_actualiza_nombrec @valold, @valnew"
      return
      end
    else
      begin
        update CARRERA set NOMBRE_C = @valnew
        where NOMBRE_C= @valold
        select @msg = "Se actualizo correctamente el registro"
        print @msg
      end
go
----ejecuta proceso
--exec sp_actualiza_nombrec @valold, @valnew 



/*==============================================================*/
/* Proceso almacenado para borrar NOMBRE_C en tabla CARRERA     */
/*==============================================================*/

create procedure sp_borra_nombrec
    (@nombrec char(40)=NULL)
    as
    declare @msg varchar(40)
    if @nombrec is NULL
      begin
      raiserror 40206
      "La ejecucion de este procedimiento es:
          exec sp_borra_nombrec @nombrec"
      return
      end
    else
      delete from CARRERA
      where NOMBRE_C = @nombrec
      select @msg= "Se borro con exito el registro"
      print @msg
      return
go
--ejecucion del proceso
--exec sp_borra_nombrec @nombrec 


/*================================================================*/
/* Proceso almacenado para borrar NOMBRE_AT en tabla AREA_TRABAJO */
/*================================================================*/

  create procedure sp_borra_at
    (@nombreat char(40)=NULL)
    as
    declare @msg varchar(40)
    if @nombreat is NULL
      begin
      raiserror 40206
      "La ejecucion de este procedimiento es:
          exec sp_borra_nombreat @nombreat"
      return
      end
    else
      delete from AREA_TRABAJO
      where NOMBRE_AT = @nombreat
      select @msg= "Se borro con exito el registro"
      print @msg
      return
go

--ejecucion del proceso
--exec sp_borra_nombreat @nombreat


/*================================================================*/
/*                        Consultas                               */
/*================================================================*/

--Consulta I
SELECT NOMBRE_P,  cast(datediff(dd,FECH_NAC,GETDATE()) / 365.25 as int) AS EDAD
FROM PROFESIONISTA, TRABAJA
WHERE(PROFESIONISTA.ID_PROF=TRABAJA.ID_PROF) 
AND GRADO_AC = 'Licenciatura' AND SALARIO > $20000



--Consulta II
SELECT COUNT(*)
FROM UNIVERSIDAD, SE_GRADUO, TRABAJA, EMPRESA
WHERE(UNIVERSIDAD.ID_UNIV=SE_GRADUO.ID_UNIV AND
SE_GRADUO.ID_PROF=TRABAJA.ID_PROF AND
TRABAJA.ID_EMP=EMPRESA.ID_EMP) AND NOMBRE_U='UNAM' AND NOMBRE_E ='Sony'


--Consulta III
SELECT NOMBRE_P, DIRECCION_P, PUESTO_E, SALARIO
FROM PROFESIONISTA, TRABAJA, EMPRESA, TIENE, AREA_TRABAJO
WHERE (PROFESIONISTA.ID_PROF=TRABAJA.ID_PROF AND 
      TRABAJA.ID_EMP= EMPRESA.ID_EMP AND EMPRESA.ID_EMP=TIENE.ID_EMP AND
      TIENE.ID_AT= AREA_TRABAJO.ID_AT) AND NOMBRE_AT= 'Educacion'



 --Consulta IV
SELECT COUNT(*)
FROM PROFESIONISTA, TRABAJA, EMPRESA, TIENE, AREA_TRABAJO
WHERE (PROFESIONISTA.ID_PROF=TRABAJA.ID_PROF AND 
      TRABAJA.ID_EMP= EMPRESA.ID_EMP AND EMPRESA.ID_EMP=TIENE.ID_EMP AND
      TIENE.ID_AT= AREA_TRABAJO.ID_AT) 
      AND NOMBRE_AT= 'Agricultura y desarrollo rural' AND GRADO_AC='Doctorado'



--Consulta V
SELECT NOMBRE_U, DOMICILIO_U, NOMBRE_C
FROM UNIVERSIDAD, CARRERA
WHERE ID_UNIV
IN (SELECT ID_UNIV
    FROM SE_IMPARTE
    WHERE ID_CARR
    IN (SELECT ID_CARR
        FROM CARRERA))

--Consulta VI
SELECT NOMBRE_E, DIRECCION_E  
FROM EMPRESA

/*================================================================*/
/*       Borrado de la base de datos                              */
/*================================================================*/

use master
go

drop database SISTEMA_PROFESIONISTAS 
go 


