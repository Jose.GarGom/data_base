 
/*================================*/
/*========== Equipo 4 ============*/
   --CORONA HERNÁNDEZ CARLOS ALBERTO
   --GARCÍA GÓMEZ JOSÉ MANUEL   

use master
go

create database SISTEMA_REDES
go

use SISTEMA_REDES
go
/*==============================================================*/
/* Table: COMPUTADORAS                                          */
/*==============================================================*/
create table COMPUTADORAS 
(
   ID_COMP              varchar(10)                    not null,
   ID_IMPRESORA         varchar(10)                    null,
   ID_RED               varchar(10)                    null,
   ID_MARCA             varchar(10)                    null,
   ID_INST              varchar(10)                    null,
   TAMMEMO              varchar(20)                    null,
   MODELOC              varchar(20)                    null,
   constraint PK_COMPUTADORAS primary key (ID_COMP)
)
go
/*==============================================================*/

/*==============================================================*/
/* Index: COMPUTADORAS_PK                                       */
/*==============================================================*/
create unique index COMPUTADORAS_PK on COMPUTADORAS (
ID_COMP ASC
)
go

/*==============================================================*/
/* Index: POSEE_FK                                              */
/*==============================================================*/
create index POSEE_FK on COMPUTADORAS (
ID_INST ASC
)
go

/*==============================================================*/
/* Index: USA_FK                                                */
/*==============================================================*/
create index USA_FK on COMPUTADORAS (
ID_IMPRESORA ASC
)
go


/*==============================================================*/
/* Index: SE_CONECTAN_FK                                        */
/*==============================================================*/
create index SE_CONECTAN_FK on COMPUTADORAS (
ID_RED ASC
)
go

create index ES_DE_FK on COMPUTADORAS (
ID_MARCA ASC
)
go

/*==============================================================*/
/* Insert values: COMPUTADORAS                                  */
/*==============================================================*/
          /*table COMPUTADORAS(ID_COMP, ID_IMPRESORA,ID_RED,ID_MARCAID_INST,TAMMEMO,MODELOC )*/
insert into COMPUTADORAS values("comp001","imp004","red009","mar003","inst004","4 GB","Xs-455")
go

insert into COMPUTADORAS values("comp002","imp007","red007","mar002","inst010","2 GB","Xv-367")
go

insert into COMPUTADORAS values("comp003","imp001","red005","mar003","inst007","4 GB","Xh-721")
go

insert into COMPUTADORAS values("comp004","imp010","red008","mar005","inst002","8 GB","Xs-567")
go 

insert into COMPUTADORAS values("comp005","imp003","red001","mar006","inst008","2 GB","Xs-231")
go

insert into COMPUTADORAS values("comp006","imp009","red006","mar003","inst003","6 GB","Xa-459")
go

insert into COMPUTADORAS values("comp007","imp008","red004","mar007","inst009","4 GB","Xd-370")
go

insert into COMPUTADORAS values("comp008","imp005","red003","mar003","inst001","8 GB","Xa-732")
go

insert into COMPUTADORAS values("comp009","imp002","red010","mar001","inst005","12 GB","Xh-965")

insert into COMPUTADORAS values("comp010","imp006","red002","mar001","inst006","8 GB","Xd-801")
go

/*==============================================================*/
/* Table: IMPRESORA                                             */
/*==============================================================*/
create table IMPRESORA 
(
   ID_IMPRESORA         varchar(10)                    not null,
   ID_TIPOIM            varchar(10)                    not null,
   ID_RED               varchar(10)                    not null,
   VELOCIDADIM          varchar(15)                    not null,
   constraint PK_IMPRESORA primary key (ID_IMPRESORA)
)
go

/*==============================================================*/
/* Index: IMPRESORA_PK                                          */
/*==============================================================*/
create unique index IMPRESORA_PK on IMPRESORA (
ID_IMPRESORA ASC
)
go
/*==============================================================*/
/* Index: ACCESA_FK                                             */
/*==============================================================*/
create index ACCESA_FK on IMPRESORA (
ID_RED ASC
)
go

create index ES_TIPO_FK on IMPRESORA (
ID_TIPOIM ASC
)
go

/*==============================================================*/
/* Insert values: IMPRESORA                                     */
/*==============================================================*/

insert into IMPRESORA values("imp001","tip001","red005","20 PPM")
go

insert into IMPRESORA values("imp002","tip003","red010","15 PPM")
go

insert into IMPRESORA values("imp003","tip003","red001","25 PPM")
go

insert into IMPRESORA values("imp004","tip001","red009","10 PPM")
go

insert into IMPRESORA values("imp005","tip001","red003","22 PPM")
go

insert into IMPRESORA values("imp006","tip002","red002","15 PPM")
go

insert into IMPRESORA values("imp007","tip001","red007","20 PPM")
go

insert into IMPRESORA values("imp008","tip003","red004","25 PPM")
go

insert into IMPRESORA values("imp009","tip003","red006","15 PPM")
go

insert into IMPRESORA values("imp010","tip001","red008","22 PPM")
go


/*==============================================================*/
/* Table: INSTITUCION                                           */
/*==============================================================*/
create table INSTITUCION 
(
   ID_INST              varchar(10)                    not null,
   ID_RED               varchar(10)                    not null,
   NOMBREI              char(30)                       not null,
   DOMICILIOI           varchar(80)                    not null,
   constraint PK_INSTITUCION primary key (ID_INST)
)
go

/*==============================================================*/
/* Index: INSTITUCION_PK                                        */
/*==============================================================*/
create unique index INSTITUCION_PK on INSTITUCION (
ID_INST ASC
)
go
/*==============================================================*/
/* Index: PERETENECE_FK                                         */
/*==============================================================*/
create index PERETENECE_FK on INSTITUCION (
ID_RED ASC
)
go

/*==============================================================*/
/* Insert values: INSTITUCION                                   */
/*==============================================================*/
insert into INSTITUCION values("inst001","red010","Fac. Ingenieria","Coyoacan,CDMX")
go

insert into INSTITUCION values("inst002","red008","Fac. Medicina","Avenida Universidad 3000, Copilco El Bajo, CDMX")
go

insert into INSTITUCION values("inst003","red010","Fac. Ciencias","Circuito Exterior s/n, Coyoacan, CDMX")
go

insert into INSTITUCION values("inst004","red009","Fac. Administracion.","Av Universidad 3000, CDMX")
go

insert into INSTITUCION values("inst005","red010","Fac. Arquitectura","Cd. Universitaria,CDMX")
go

insert into INSTITUCION values("inst006","red002","Fac. Derecho","Cd. Universitaria, 04510 Coyoacan, CDMX")
go

insert into INSTITUCION values("inst007","red005","Fac. Filosofia","Cd. Universitaria, s/n, 04510 Coyoacan, CDMX")
go

insert into INSTITUCION values("inst008","red001","Fac. Economia","Circuito Interior S/N, Cd. Universitaria, CDMX")
go

insert into INSTITUCION values("inst009","red004","Fac. Psicologia","Avenida Universidad 3004, Del. Coyoacan, Col. Copilco")
go

insert into INSTITUCION values("inst010","red007","Fac. Politica","Circuito Mario de La Cueva s/n, Cd. Universitaria, Coyoacan, CDMX")
go

/*==============================================================*/
/* Table: MARCAC                                                */
/*==============================================================*/
create table MARCAC 
(
   ID_MARCA             varchar(10)                    not null,
   MARCA                char(20)                       not null unique,
   constraint PK_MARCAC primary key (ID_MARCA)
)
go
/*==============================================================*/
/* Index: MARCAC_PK                                             */
/*==============================================================*/
create unique index MARCAC_PK on MARCAC (
ID_MARCA ASC
)
go

/*==============================================================*/
/* Insert values: MARCAC                                        */
/*==============================================================*/
insert into MARCAC values("mar001","SONY VAIO")
go

insert into MARCAC values("mar002","DELL")
go

insert into MARCAC values("mar003","HP")
go

insert into MARCAC values("mar004","APPLE")
go

insert into MARCAC values("mar005","LENOVO")
go

insert into MARCAC values("mar006","THOSHIBA")
go

insert into MARCAC values("mar007","SAMSUMG")
go

/*==============================================================*/
/* Table: PAQUETESW                                             */
/*==============================================================*/
create table PAQUETESW 
(
   ID_PAQSW             varchar(10)                    not null,
   NOMBREPS             char(20)                       not null,
   constraint PK_PAQUETESW primary key (ID_PAQSW)
)
go
/*==============================================================*/
/* Index: PAQUETESW_PK                                          */
/*==============================================================*/
create unique index PAQUETESW_PK on PAQUETESW (
ID_PAQSW ASC
)
go

/*==============================================================*/
/* Insert values: PAQUETESW                                     */
/*==============================================================*/

insert into PAQUETESW values("paqSW001","Office")
go

insert into PAQUETESW values("paqSW002","Photoshop")
go

insert into PAQUETESW values("paqSW003","Autocad")
go

insert into PAQUETESW values("paqSW004","Sybase")
go

insert into PAQUETESW values("paqSW005","Android Studio")
go

insert into PAQUETESW values("paqSW006","After Effects")
go

insert into PAQUETESW values("paqSW007","Plague Inc.")
go

insert into PAQUETESW values("paqSW008","Eclipse")
go

insert into PAQUETESW values("paqSW009","Corel")
go

insert into PAQUETESW values("paqSW010","CivilCad")
go


/*==============================================================*/
/* Table: REDES                                                 */
/*==============================================================*/
create table REDES 
(
   ID_RED               varchar(10)                    not null,
   NOMBRER              char(20)                       not null,
   constraint PK_REDES primary key (ID_RED)
)
go
/*==============================================================*/
/* Index: REDES_PK                                              */
/*==============================================================*/
create unique index REDES_PK on REDES (
ID_RED ASC
)
go

/*==============================================================*/
/* Insert values: REDES                                         */
/*==============================================================*/

insert into REDES values("red001","Red_Biblioteca")
go

insert into REDES values("red002","Red_Puma")
go

insert into REDES values("red003","Red_Direccion")
go

insert into REDES values("red004","Red_Castillo")
go

insert into REDES values("red005","Red_UNAM")
go

insert into REDES values("red006","Red_Maestros")
go

insert into REDES values("red007","Red_Privada")
go

insert into REDES values("red008","Red_Laboratorio")
go

insert into REDES values("red009","Red_Instituto")
go 

insert into REDES values("red010","Red_Estudiantes")
go 


/*==============================================================*/
/* Table: TIENE                                                 */
/*==============================================================*/
create table TIENE 
(
   ID_COMP              varchar(10)                    not null,
   ID_PAQSW             varchar(10)                    not null,
   constraint PK_TIENE primary key clustered (ID_COMP, ID_PAQSW)
)
go

/*==============================================================*/
/* Index: TIENE_FK                                              */
/*==============================================================*/
create index TIENE_FK on TIENE (
ID_COMP ASC
)
go
/*==============================================================*/
/* Index: TIENE2_FK                                             */
/*==============================================================*/
create index TIENE2_FK on TIENE (
ID_PAQSW ASC
)
go

/*==============================================================*/
/* Insert values: TIENE                                         */
/*==============================================================*/


insert into TIENE values("comp001","paqSW001")
go

insert into TIENE values("comp002","paqSW003")
go

insert into TIENE values("comp003","paqSW004")
go 

insert into TIENE values("comp004","paqSW009")
go 

insert into TIENE values("comp005","paqSW001")
go

insert into TIENE values("comp006","paqSW010")
go

insert into TIENE values("comp007","paqSW002")
go 

insert into TIENE values("comp008","paqSW007")
go

insert into TIENE values("comp009","paqSW006")
go

insert into TIENE values("comp010","paqSW008")

/*==============================================================*/
/* Table: TIPOIM                                                */
/*==============================================================*/
create table TIPOIM 
(
   ID_TIPOIM            varchar(10)                    not null,
   TIPO                 char(30)                       not null unique,
   constraint PK_TIPOIM primary key (ID_TIPOIM)
)
go
/*==============================================================*/
/* Index: TIPOIM_PK                                             */
/*==============================================================*/
create unique index TIPOIM_PK on TIPOIM (
ID_TIPOIM ASC
)
go

insert into TIPOIM values("tip001","INYECCION")
go

insert into TIPOIM values("tip002","TERMICA")
go

insert into TIPOIM values("tip003","LASER")
go


alter table COMPUTADORAS
   add constraint FK_COMPUTAD_ES_DE_MARCAC foreign key (ID_MARCA)
      references MARCAC (ID_MARCA)
go      
      
alter table COMPUTADORAS
   add constraint FK_COMPUTAD_POSEE_INSTITUC foreign key (ID_INST)
      references INSTITUCION (ID_INST)
go      
     
alter table COMPUTADORAS
   add constraint FK_COMPUTAD_SE_CONECT_REDES foreign key (ID_RED)
      references REDES (ID_RED)
go

alter table COMPUTADORAS
   add constraint FK_COMPUTAD_USA_IMPRESOR foreign key (ID_IMPRESORA)
      references IMPRESORA (ID_IMPRESORA)
go      
     
alter table IMPRESORA
   add constraint FK_IMPRESOR_ACCESA_REDES foreign key (ID_RED)
      references REDES (ID_RED)
go      
     

alter table IMPRESORA
   add constraint FK_IMPRESOR_ES_TIPO_TIPOIM foreign key (ID_TIPOIM)
      references TIPOIM (ID_TIPOIM)
go

alter table INSTITUCION
   add constraint FK_INSTITUC_PERETENEC_REDES foreign key (ID_RED)
      references REDES (ID_RED)
go

alter table TIENE
   add constraint FK_TIENE_TIENE_COMPUTAD foreign key (ID_COMP)
      references COMPUTADORAS (ID_COMP)
go

alter table TIENE
   add constraint FK_TIENE_TIENE2_PAQUETES foreign key (ID_PAQSW)
      references PAQUETESW (ID_PAQSW)
go

/************************************************/
--proceso para insertar un valor en MARCAC

create proc proc_inserta_marcac (@idmarcac varchar(10) = NULL, @nombre varchar(10) = NULL)
as
declare @msg varchar(35)
if @idmarcac is NULL or @nombre is NULL
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_inserta_marcac @idmarcac, @nombre"
    return
    end  
else
    insert into MARCAC values (@idmarcac, @nombre)
   select @msg ="En el registro se ha insertado"
   print @msg  --Imprime mensaje
    return
go
--ejecuta proceso
--exec proc_inserta_marcac @idmarcac = "mar008", @nombre = "MICROSFT" 

/*****************************************************/
--proceso para insertar un valor en TIPOIM

create proc proc_inserta_tipoim (@idtipoim varchar(10) = NULL, @nombre varchar(10) = NULL)
as
declare @msg varchar(35)
if @idtipoim is NULL or @nombre is NULL
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_inserta_tipoim @idtipoim, @nombre"
    return
    end  
else
    insert into TIPOIM values (@idtipoim, @nombre)
   select @msg ="En el registro se ha insertado"
   print @msg  --Imprime mensaje
    return
go

--ejecuta proceso
--exec proc_inserta_tipoim @idtipoim = "tip004", @nombre = "3D" 

/*****************************************************/
--proceso para actualizar el valor de ID_MARCA en MARCAC

create proc proc_actualiza_marcac_id (@valold varchar(15) = NULL, @valnew varchar(15) = NULL)
as
declare @msg varchar(35)
if @valold is NULL or @valnew is NULL
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_actualiza_marcac_id @valold, @valnew"
    return
    end  
else
    begin 
   update MARCAC set ID_MARCA = @valnew
   where ID_MARCA = @valold
   select @msg ="En el registro se ha actualizado"
   print @msg  --Imprime mensaje
    end
    return
go

--ejecuta proceso
--exec proc_actualiza_marcac_id @valold = "mar008", @valnew ="mar080" 

/*****************************************************/
--proceso para actualizar el valor de MARCA en MARCAC

create proc proc_actualiza_marcac_marca (@valold varchar(15) = NULL, @valnew varchar(15) = NULL)
as
declare @msg varchar(35)
if @valold is NULL or @valnew is NULL
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_actualiza_marcac_marca @valold, @valnew"
    return
    end  
else
    begin 
   update MARCAC set MARCA = @valnew
   where MARCA = @valold
   select @msg ="En el registro se ha actualizado"
   print @msg  --Imprime mensaje
    end
    return
go
--ejecuta proceso
--exec proc_actualiza_marcac_marca @valold = "MICROSOFT", @valnew ="ALIENWARE"

/*****************************************************/
--proceso para actualizar el valor de ID_TIPOIM en TIPOIM

create proc proc_actualiza_tipoim_id (@valold varchar(15) = NULL, @valnew varchar(15) = NULL)
as
declare @msg varchar(35)
if @valold is NULL or @valnew is NULL
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_actualiza_marcac_id @valold, @valnew"
    return
    end  
else
    begin 
   update TIPOIM set ID_TIPOIM = @valnew
   where ID_TIPOIM = @valold
   select @msg ="En el registro se ha actualizado"
   print @msg  --Imprime mensaje
    end
    return
go
--ejecuta proceso
--exec proc_actualiza_tipoim_id @valold = "tip004", @valnew ="tip040"

/*****************************************************/
--proceso para actualizar el valor de TIPO en TIMPOIM

create proc proc_actualiza_tipoim_tipo (@valold varchar(15) = NULL, @valnew varchar(15) = NULL)
as
declare @msg varchar(35)
if @valold is NULL or @valnew is NULL
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_actualiza_marcac_marca @valold, @valnew"
    return
    end  
else
    begin 
   update TIPOIM set TIPO = @valnew
   where TIPO = @valold
   select @msg ="En el registro se ha actualizado"
   print @msg  --Imprime mensaje
    end
    return
go
--ejecuta proceso
--exec proc_actualiza_tipoim_tipo @valold = "3D", @valnew ="otra"

/*****************************************************/
--proceso para borra un registro segun su id en MARCAC

create proc proc_borra_marcac (@id_marca varchar(15) = NULL)
as
declare @msg varchar(35)
if @id_marca is NULL 
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_borra_marcac @id_marca"
    return
    end  
else
    delete from MARCAC
    where ID_MARCA = @id_marca
   select @msg ="En el registro se ha borrado"
   print @msg  --Imprime mensaje
    return
go
--ejecuta proceso
--exec proc_borra_marcac  @id_marca = "mar080"

/*****************************************************/
--proceso para borra un registro segun su id en TIPOIM

create proc proc_borra_tipoim (@id_tipo varchar(15) = NULL)
as
declare @msg varchar(35)
if @id_tipo is NULL 
    begin
      raiserror 20001
      "La ejecucion de este procedimiento es:
      exec proc_borra_marcac @id_tipo"
    return
    end  
else
    delete from TIPOIM
    where ID_TIPOIM = @id_tipo
   select @msg ="En el registro se ha borrado"
   print @msg  --Imprime mensaje
    return
go

--ejecuta proceso
--exec proc_borra_tipoim  @id_tipo = "tip040"


/*****************************************/
-- trigger para actualización en COMPUTADORAS cuando se modifica el 
-- ID_MARCA en MARCAC

create trigger cascade_update_marcac
on MARCAC
for update as
if update(ID_MARCA)
begin
     update COMPUTADORAS
           set ID_MARCA = inserted.ID_MARCA
           from COMPUTADORAS, deleted, inserted
           where deleted.ID_MARCA = COMPUTADORAS.ID_MARCA
end
go
/*****************************************/
-- trigger para actualización en IMPRESORA cuando se modifica el 
-- ID_TIPOIM en TIPOIM

create trigger cascade_update_tipoim
on TIPOIM
for update as
if update(ID_TIPOIM)
begin
     update IMPRESORA
           set ID_TIPOIM = inserted.ID_TIPOIM
           from IMPRESORA, deleted, inserted
           where deleted.ID_TIPOIM = IMPRESORA.ID_TIPOIM
end
go

/*****************************************/
-- trigger para borrado en cascada en TIENE cuando se borra un 
-- registro en COMPUTADORAS

create trigger delcascadetienecomp
on COMPUTADORAS
for delete 
as 
delete TIENE
from TIENE, deleted 
where TIENE.ID_COMP= deleted.ID_COMP
/* borra el renglón ID_COMP especificado de TIENE*/
go
/*****************************************/
-- trigger para borrado en cascada en TIENE cuando se borra un 
-- registro en PAQUETESW

create trigger delcascadetienepaq
on PAQUETESW
for delete 
as 
delete TIENE
from TIENE, deleted 
where TIENE.ID_PAQSW= deleted.ID_PAQSW
/* borra el renglón ID_PAQSW especificado de TIENE*/
go

/* consulta 1*/
SELECT MARCA, MODELOC, TAMMEMO
FROM MARCAC,COMPUTADORAS, INSTITUCION
WHERE (MARCAC.ID_MARCA=COMPUTADORAS.ID_MARCA
AND COMPUTADORAS.ID_INST=INSTITUCION.ID_INST)
AND NOMBREI='Fac. Ingenieria'
go

/* consulta 2*/
SELECT NOMBREI, DOMICILIOI
FROM INSTITUCION
WHERE ID_INST
IN (SELECT ID_INST
FROM COMPUTADORAS
WHERE ID_COMP
IN (SELECT ID_COMP
FROM TIENE
WHERE ID_PAQSW
IN (SELECT ID_PAQSW
FROM PAQUETESW
WHERE NOMBREPS='Office')))
go

/* consulta 3*/
SELECT NOMBREI Institucion
FROM INSTITUCION
WHERE ID_RED 
IN (SELECT ID_RED FROM INSTITUCION
WHERE NOMBREI = 'Fac. Derecho')
go 